import React from 'react';
import './App.css';
import { 
  BrowserRouter as Router,
  Switch,
} from "react-router-dom"
import Home from './Components/Home/Home'
import Categories from './Components/Categories/Categories'
import Search from './Components/Search/Search'
import Jokes from './Components/Jokes/Jokes'
import Nav from './Components/Nav/Nav'
import AuthRoute from './Components/AuthRoute'
import AuthContext from './Components/AuthContext/AuthContext'

// #This component is an example of a functional component
function App() {

  return (
    <AuthContext>
      <div className='App'>
        <Router>
          <Nav/>
          <Switch>
            <AuthRoute exact path="/" component={Home}/>
            <AuthRoute exact path="/categories" component={Categories}/>
            <AuthRoute exact path="/search" component={Search}/>
            <AuthRoute exact path="/jokes" component={Jokes}/>
          </Switch>
        </Router>
      </div>
    </AuthContext>
  );
}

export default App;
