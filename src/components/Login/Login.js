import React from 'react';
import { 
    UserContext, 
    fakeAuthenticationRequest,
    fakeSignUpRequest,
    containsNoEmailSymbol,
    containsNoUpperCase, 
    containsNoSpecialCharacter,
    containsNoNumbers,
    lengthIsNotValid,
} from '../../Helper/common'
import './Login.css';

export default function Login() {

    const [nameState, setNameState] = React.useState('')
    const [passwordState, setPasswordState] = React.useState('')
    const [errorState, setErrorState] = React.useState({ 
        loginAttempt: { failed: '', count: 0 }, 
        username: '', 
        password: { containsUpper: '', containsSpecial: '', containsNumbers: '', length: '', }
    })
    const [passwordVisibilityState, setPasswordVisibilityState] = React.useState(true)
    const [selectedRadioButton, setSelectedRadioButton] = React.useState('')
    const [loading, setLoading] = React.useState(false)
    const context = React.useContext(UserContext);

    // #This handleSubmit demonstrates proper event handling
    const handleSubmit = (event) => {
        event.preventDefault()
        setLoading(true)
        if ( selectedRadioButton === 'login') { 
            fakeAuthenticationRequest(nameState, passwordState)
            .then(
                response => 
                {   
                    if(response === true) {
                    let newErrorState = {
                        loginAttempt: { failed: "Login Successful!", count: 0},
                        username: errorState.username,
                        password: errorState.password,
                    }
                    setErrorState(newErrorState)
                    
                    context.login()
                    } else {
                        let newErrorState = { 
                            loginAttempt: { failed: "Whoops! We didn't find a user by the email or password!", count: errorState.loginAttempt.count + 1},
                            username: '',
                            password: { containsUpper: '', containsSpecial: '', containsNumbers: '', length: '', },
                        }
                        setErrorState(newErrorState)
                    }
                    setLoading(false)
                })
        } else if ( selectedRadioButton === 'signUp' ) { 
            fakeSignUpRequest(nameState, passwordState)
            .then(
                (response) => { if(JSON.parse(response)) {
                    let newErrorState = {
                        loginAttempt: { failed: "Sign Up Successful!", count: 0},
                        username: errorState.username,
                        password: errorState.password,
                    }
                    setErrorState(newErrorState)
                    
                    context.login()
                    setLoading(false)
                    } else {
                        let newErrorState = { 
                            loginAttempt: { failed: "The sign up process didn't work!", count: 0},
                            username: '',
                            password: { containsUpper: '', containsSpecial: '', containsNumbers: '', length: '', },
                        }
                        setErrorState(newErrorState)
                        setLoading(false)
                        }
                }   )
        }
        
    }

    // I've edited these two into ternary syntax. While it's not the cleanest, it at least will throw a better error than
    // simply falling out of an "if / else if" with no final "else", as it was.
    
    const handleCredentialChange = (event) => {
        event.target.name === 'username' ? setNameState(event.target.value) : setPasswordState(event.target.value)
    }

    const handleRadioButtonChange = (event) => {
        event.target.value === 'login' ? setSelectedRadioButton('login') : setSelectedRadioButton('signUp')
    }

    // const handleValidation = (event) => {
    //     let errors = { 
    //         loginAttempt: errorState.loginAttempt, 
    //         username: '',
    //         password: { 
    //             containsUpper: '', 
    //             containsSpecial: '', 
    //             containsNumbers: '', 
    //             length: '',
    //         }
    //     }

    //     if (event.target.name === 'username') {
    //         if (containsNoEmailSymbol(event.target.value) === true) {
    //             errors.username = 'Whoops! Please add a valid email'
    //         }
    //     } else if (event.target.name ==='password') {
    //         if (containsNoUpperCase(event.target.value) === true) {
    //             errors.password.containsUpper = 'Whoops! You need at least ONE upper case letter in your password.'
    //         } 
    //         if (containsNoSpecialCharacter(event.target.value) === true) {
    //             errors.password.containsSpecial = 'Whoops! You need at least ONE special character in your password (ie: ! @ # $ % % ^ & * ).' 
    //         } 
    //         if (containsNoNumbers(event.target.value) === true) {
    //             errors.password.containsNumbers = 'Whoops! You need at least ONE number in your password'
    //         } 
    //         if (lengthIsNotValid(event.target.value) === true) {
    //             errors.password.length = 'Whoops! Your password must be be at least 6, and no more than 11, characters long.'
    //         }
    //     }
    //     setErrorState(errors)
    // }

    const handleUsernameValidation = (event) => {
        let errors = { 
            loginAttempt: errorState.loginAttempt, 
            username: '',
            password: { 
                containsUpper: errorState.password.containsUpper, 
                containsSpecial: errorState.password.containsSpecial, 
                containsNumbers: errorState.password.containsNumbers, 
                length: errorState.password.length,
            }
        }

        if (containsNoEmailSymbol(event.target.value) === true) {
            errors.username = 'Whoops! Please add a valid email'
        }
        
        setErrorState(errors)
    }

    const handlePasswordValidation = (event) => {
        let errors = { 
            loginAttempt: errorState.loginAttempt, 
            username: errorState.username,
            password: { 
                containsUpper: '', 
                containsSpecial: '', 
                containsNumbers: '', 
                length: '',
            }
        }

        if (containsNoUpperCase(event.target.value) === true) {
            errors.password.containsUpper = 'Whoops! You need at least ONE upper case letter in your password.'
        } 
        if (containsNoSpecialCharacter(event.target.value) === true) {
            errors.password.containsSpecial = 'Whoops! You need at least ONE special character in your password (ie: ! @ # $ % % ^ & * ).' 
        } 
        if (containsNoNumbers(event.target.value) === true) {
            errors.password.containsNumbers = 'Whoops! You need at least ONE number in your password'
        } 
        if (lengthIsNotValid(event.target.value) === true) {
            errors.password.length = 'Whoops! Your password must be be at least 6, and no more than 11, characters long.'
        }

        setErrorState(errors)
    }

    const togglePasswordShow = (event) => {
        event.preventDefault()
        setPasswordVisibilityState(!passwordVisibilityState)
    }

    const redText = {
        color: 'red'
    }

    return (
        /*                                                                                              *
        * NOTE for <form> handeling: Typically, I would use a popular third-party package like Formik,  *
        * however, in this example, I've decided to use React forms, for the purposes of the evaluation *
        *                                                                                               */
        loading ? <div data-test-id="loadingIndicator">Loading...</div> :
        <form onSubmit={handleSubmit}>
            { errorState.loginAttempt.failed !== '' && <div style={redText} data-test-id="errorLoginAttempt"> {errorState.loginAttempt.failed} </div> }
            { errorState.loginAttempt.count !== 0 && <div style={redText} data-test-id="errorCount"> Failed Attempts: {errorState.loginAttempt.count} </div> }
            <label>
                Enter your email (it's 'dallas@email.com')
                <input
                    data-test-id='usernameInput'
                    type='text'
                    name='username'
                    value={nameState}
                    onChange={handleCredentialChange}
                    onBlur={handleUsernameValidation}
                />
            </label>
            { errorState.username && <div style={redText} data-test-id="errorUsername"> {errorState.username} </div> }

            <label>
                Enter your password (it's 'Test12@')
                <input
                    data-test-id='passwordInput'
                    type={ passwordVisibilityState === true ? 'password' : 'text'}
                    name='password'
                    value={passwordState}
                    onChange={handleCredentialChange}
                    onBlur={handlePasswordValidation}
                />
            </label>
            { errorState.password.containsUpper !== '' && <div style={redText} data-test-id="errorContainsUpper"> {errorState.password.containsUpper} </div> }
            { errorState.password.containsSpecial !== '' && <div style={redText} data-test-id="errorContainsSpecial"> {errorState.password.containsSpecial} </div> }
            { errorState.password.containsNumbers !== '' && <div style={redText} data-test-id="errorContainsNumbers" > {errorState.password.containsNumbers} </div> }
            { errorState.password.length !== '' && <div style={redText} data-test-id="errorLength"> {errorState.password.length} </div> }
            <button
                data-test-id='togglePasswordShow'
                type='text'
                onClick={togglePasswordShow}
            > Show Password </button>
            {selectedRadioButton !== '' && 
                <button
                    data-test-id='authenticationButton'
                    type='submit'
                    onClick={handleSubmit}
                    disabled={ 
                        errorState.username !== '' 
                        || errorState.password.containsUpper !== ''
                        || errorState.password.containsSpecial !== ''
                        || errorState.password.containsNumbers !== ''
                        || errorState.password.length !== '' 
                        || nameState === ''
                        || passwordState === ''
                        || selectedRadioButton === ''
                    }>
                        {selectedRadioButton === 'login' ? 'Login' : 'Sign Up'} 
                </button>
            }
            
            
            <label>
                <input
                data-test-id="radioLoginButton"
                type="radio"
                name="loginOrSignUp"
                value="login"
                onChange={handleRadioButtonChange}
                checked={selectedRadioButton === 'login'}
                />
                Login
            </label>
            <label>
                <input
                data-test-id="radioSignUpButton"                
                type="radio"
                name="loginOrSignUp"
                value="signUp"
                onChange={handleRadioButtonChange}
                checked={selectedRadioButton === 'signUp'}
                />
                Sign Up
            </label>
        </form>
    )
}