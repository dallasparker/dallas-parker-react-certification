// *** Note for Testing ***
// *** I have created two easy CLI tools to use in the package.json ***
// *** One is the "npm run coverage" command, which runs a testing report with 'watch' disabled ***
// *** The other is "npm run slim-coverage" which runs a testing report excluding the index.js and serviceWorker.js files ***

import React from "react";
import { shallow, mount } from 'enzyme';
import { unmountComponentAtNode } from "react-dom";
import Login from './Login'
import { fakeAuthenticationRequest } from '../../Helper/common'
import renderer from 'react-test-renderer';

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

/************* Potential Test to be Made ******************/

it('renders correctly', () => {
  const tree = renderer.create(<Login/>).toJSON();
  expect(tree).toMatchSnapshot();
});

// on handleSubmit
it('succesfully performs fake login authentication', (done)=>{
    const LoginComponent = mount(<Login />);

    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('change', {
        target: {
          name: 'username',
          value: 'dallas@email.com',
        },
      });
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('change', {
        target: {
            name: 'password',
            value: 'Test12@',
        },
    });

    const username = LoginComponent.find('input[data-test-id="usernameInput"]').props().value
    expect(username === 'dallas@email.com')

    const password = LoginComponent.find('input[data-test-id="passwordInput"]').props().value
    expect(password === 'Test12@')

    const response = fakeAuthenticationRequest(username, password)
    .then(
        response => 
        {
            if(response === true) {
                return "Login Successful!"
            } else {
                return "Whoops! We didn't find a user by the email or password!"
            }
        })

    expect( response ==='Login Successful!')
    done()
})

// on handleSubmit
it('successfully renders loading text upon handleSubmit', (done) => {
  const LoginComponent = mount(<Login />);

    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('change', {
        target: {
          name: 'username',
          value: 'dallas@email.com',
        },
      });
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('change', {
        target: {
            name: 'password',
            value: 'Test12@',
        },
    });

    LoginComponent.find('[data-test-id="radioLoginButton"]').simulate('click')

    LoginComponent.find('[data-test-id="authenticationButton"]').simulate('click', {
      preventDefault: function() {
        return 1
      }
    })

    expect(LoginComponent.find('[data-test-id="loadingIndicator"]').text()).toEqual("Loading...")
    done()
})

// on handleSubmit
it('gracefully fails fake login authentication', (done)=>{
    const LoginComponent = shallow(<Login />);

    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('change', {
        target: {
          name: 'username',
          value: 'wrong@email.com',
        },
      });
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('change', {
        target: {
            name: 'password',
            value: 'Wrongpass1!',
        },
    });

    const username = LoginComponent.find('input[data-test-id="usernameInput"]').props().value
    expect(username === 'wrong@email.com')

    const password = LoginComponent.find('input[data-test-id="passwordInput"]').props().value
    expect(password === 'Wrongpass1!')

    const response = fakeAuthenticationRequest(username, password)
    .then(
        response => 
        {
            if(response === true) {
                return "Login Successful!"
            } else {
                return "Whoops! We didn't find a user by the email or password!"
            }
        })
    .catch( 
        error => 
        {
            return "Uh oh, there was a problem! See your console for error info"
        })

    expect(response === "Whoops! We didn't find a user by the email or password!")
    done()
})

// on handleSubmit
it('succesfully performs fake sign up authentication', (done)=>{
    const LoginComponent = shallow(<Login />);

    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('change', {
        target: {
          name: 'username',
          value: 'newUser@email.com',
        },
      });
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('change', {
        target: {
            name: 'password',
            value: 'Newpassword12@',
        },
    });

    const username = LoginComponent.find('input[data-test-id="usernameInput"]').props().value
    expect(username === 'newUser@email.com')

    const password = LoginComponent.find('input[data-test-id="passwordInput"]').props().value
    expect(password === 'Newpassword12@')

    const response = fakeAuthenticationRequest(username, password)
    .then(
        (response) => { if(JSON.parse(response)) {
                return "Sign Up Successful!"
            } else {
                return "The sign up process didn't work!"
            }
        }   )
    .catch( (error) => {
        return "Uh oh, there was a problem! See your console for error info"
    })

    expect(response ==='Sign Up Successful!')
    done()
})

// on handleSubmit
it('gracefully fails fake sign up authentication', (done)=>{
    const LoginComponent = shallow(<Login />);

    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('change', {
        target: {
          name: 'username',
          value: 'dallas@email.com',
        },
      });
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('change', {
        target: {
            name: 'password',
            value: 'Irreleventpw12@',
        },
    });

    const username = LoginComponent.find('input[data-test-id="usernameInput"]').props().value
    expect(username === 'dallas@email.com')

    const password = LoginComponent.find('input[data-test-id="passwordInput"]').props().value
    expect(password === 'Irreleventpw12@')

    const response = fakeAuthenticationRequest(username, password)
    .then(
        (response) => { if(JSON.parse(response)) {
                return "Sign Up Successful!"
            } else {
                return "The sign up process didn't work!"
            }
        }   )
    .catch( (error) => {
        return "Uh oh, there was a problem! See your console for error info"
    })

    expect(response ==="The sign up process didn't work!")
    done()
})

// on handleRadioButtonChange
it('successfully displays Login button when login radio button selected', ()=>{
    const LoginComponent = mount(<Login />);
    expect(LoginComponent.find('input[data-test-id="radioLoginButton"]').length).toEqual(1);
    const radioLoginButton = LoginComponent.find('input[data-test-id="radioLoginButton"]')
    radioLoginButton.simulate('change', {
        target: {
            value: 'login'
        }
    });
    expect(LoginComponent.find('button[data-test-id="authenticationButton"]').text()).toEqual(' Login');
})

// on handleRadioButtonChange
it('successfully displays Sign Up button when sign up radio button selected', ()=>{
    const LoginComponent = shallow(<Login />);
    expect(LoginComponent.find('input[data-test-id="radioSignUpButton"]').length).toEqual(1);
    const radioSignUpButton = LoginComponent.find('input[data-test-id="radioSignUpButton"]')
    radioSignUpButton.simulate('change', {
        target: {
            value: 'signUp'
        }
    });
    
    expect(LoginComponent.find('button[data-test-id="authenticationButton"]').text()).toEqual(' Sign Up');

})

// on togglePasswordChange
it('successfully toggles password display-ability when the "Show Password" button is clicked', ()=>{
    const LoginComponent = shallow(<Login />);
    expect(LoginComponent.find('button[data-test-id="togglePasswordShow"]').length).toEqual(1);
    const initialPasswordType = LoginComponent.find('input[data-test-id="passwordInput"]').getElement().props.type
    const passwordShowButton = LoginComponent.find('button[data-test-id="togglePasswordShow"]')
    passwordShowButton.simulate('click', {
        preventDefault: function() {
            return 1
          }
    })
    const newPasswordType = LoginComponent.find('input[data-test-id="passwordInput"]').getElement().props.type

    expect(initialPasswordType !== newPasswordType)

})

// on handleCredentialChange
it('successfully changes username credentials upon changing username input', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('change', {
      target: {
        name: 'username',
        value: 'newUserName',
      },
    });
    expect(LoginComponent.find('input[data-test-id="usernameInput"]').getElement().props.value).toEqual(
      'newUserName',
    );
});

// on handleCredentialChange
it('successfully changes password credentials upon changing password input', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('change', {
      target: {
        name: 'password',
        value: 'newPassword',
      },
    });
    expect(LoginComponent.find('input[data-test-id="passwordInput"]').getElement().props.value).toEqual(
      'newPassword',
    );
});

// on handleValidation
it('successfully confirms that there is no email symbol (@)', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('blur', {
      target: {
        name: 'username',
        value: 'dallasAtEmail.com',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorUsername"]').text()).toEqual(' Whoops! Please add a valid email ');
})

// on handleValidation
it('successfully confirms that there is an email symbol (@)', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="usernameInput"]').simulate('blur', {
      target: {
        name: 'username',
        value: 'dallasAtEmail.com',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorUsername"]')).toEqual({})
})

// on handleValidation
it('successfully confirms that there is no uppercase symbol', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'nouppercasepassword',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorContainsUpper"]').text()).toEqual(' Whoops! You need at least ONE upper case letter in your password. ')
})

// on handleValidation
it('successfully confirms that there is an uppercase symbol', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'containsUppercasepassword',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorContainsUpper"]')).toEqual({})
})

// on handleValidation
it('successfully confirms that there are no special characters', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'containsnospecialcharacters',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorContainsSpecial"]').text()).toEqual(' Whoops! You need at least ONE special character in your password (ie: ! @ # $ % % ^ & * ). ')
})

// on handleValidation
it('successfully confirms that there is at least one special character', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'containsspecialcharacter!',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorContainsSpecial"]')).toEqual({})
})

// on handleValidation
it('successfully confirms that it contains no numbers', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'containsnonumbers',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorContainsNumbers"]').text()).toEqual(' Whoops! You need at least ONE number in your password ')
})

// on handleValidation
it('successfully confirms that it contains numbers', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'contains1number',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorContainsNumbers"]')).toEqual({})
})

// on handleValidation
it('successfully confirms that username is a valid length', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'validlength',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorLength"]')).toEqual({})
})

// on handleValidation
it('successfully confirms that username is not a valid length', () => {
    const LoginComponent = shallow(<Login />);
    LoginComponent.find('input[data-test-id="passwordInput"]').simulate('blur', {
      target: {
        name: 'password',
        value: 'definatelyinvalidlength',
      },
    });
    expect(LoginComponent.find('div[data-test-id="errorLength"]').text()).toEqual(' Whoops! Your password must be be at least 6, and no more than 11, characters long. ')
})

