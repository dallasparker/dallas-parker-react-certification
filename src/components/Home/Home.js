import React from 'react'
import { Link } from "react-router-dom"

export default function Home() {
    const styles = {
        display: 'flex',
        flexDirection: 'column'
    }

    const blackFont = {
        color: 'black'
    }

    return (
        <>
            <h1 style={styles}>
                This app consists of 4 parts: The 'Home' page, 'categories', 'search', and 'jokes'.
            </h1>
            
            <h2><Link style={blackFont} data-testid="home-link" to="/">Home</Link></h2>
            <p>Where you can get organized!</p>
            <h2><Link style={blackFont} data-testid="category-link" to="/categories">Categories</Link></h2>
            <p>Find a random joke by Category.</p>
            <h2><Link style={blackFont} data-testid="search-link" to="/search">Search</Link></h2>
            <p>Search for a joke by a keyword</p>
            <h2><Link style={blackFont} data-testid="jokes-link" to="/jokes">Jokes</Link></h2>
            <p>A collection / history of jokes you have viewed and selected</p>
        </>


    )
}
