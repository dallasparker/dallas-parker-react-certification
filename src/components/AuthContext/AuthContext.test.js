/* eslint-disable no-useless-escape */
// *** Note for Testing ***
// *** I have created two easy CLI tools to use in the package.json ***
// *** One is the "npm run coverage" command, which runs a testing report with 'watch' disabled ***
// *** The other is "npm run slim-coverage" which runs a testing report excluding the index.js and serviceWorker.js files ***
import React from 'react';
import { AuthProvider } from './AuthContext'
import Joke from '../Jokes/Joke'
import { mount } from 'enzyme';
import { UserContext } from '../../Helper/common'
import Jokes from '../Jokes/Jokes';


it('sets loginStatus status to true', () => {
    const TestComponent = () => {
        const { login, loginStatus } = React.useContext(UserContext)

        return (
            <>
                <div data-test-id="loginElement">{loginStatus.toString()}</div>
                <button data-test-id="loginButtonElement" onClick={login}>Login</button>
            </>
        )
    }

    const wrapper = mount(
        <AuthProvider>
            <TestComponent />
        </AuthProvider>
    )

    expect(wrapper.find('[data-test-id="loginElement"]').text()).toEqual("false")

    wrapper.find('[data-test-id="loginButtonElement"]').simulate('click')

    expect(wrapper.find('[data-test-id="loginElement"]').text()).toEqual("true")
})

it('sets loginStatus status to true', () => {
    const TestComponent = () => {
        const { login, logout, loginStatus } = React.useContext(UserContext)

        return (
            <>
                <div data-test-id="loginStatusElement">{loginStatus.toString()}</div>
                <button data-test-id="loginButtonElement" onClick={login}>Login</button>
                <button data-test-id="logoutButtonElement" onClick={logout}>Login</button>
            </>
        )
    }

    const wrapper = mount(
        <AuthProvider>
            <TestComponent />
        </AuthProvider>
    )

    expect(wrapper.find('[data-test-id="loginStatusElement"]').text()).toEqual("false")

    wrapper.find('[data-test-id="loginButtonElement"]').simulate('click')

    expect(wrapper.find('[data-test-id="loginStatusElement"]').text()).toEqual("true")

    wrapper.find('[data-test-id="logoutButtonElement"]').simulate('click')

    expect(wrapper.find('[data-test-id="loginStatusElement"]').text()).toEqual("false")
})

it('updates saved jokes', () => {

    const TestComponent = () => {
        const {savedJokes} = React.useContext(UserContext)

        const mockSavedJokes = {
            old: [
                {text: 'mock joke 1', category: 'mock category 1', time: '21:00:00'}, 
                {text: 'mock joke 2', category: 'mock category 2', time: '22:00:00'}
            ], 
            updated: [
                {text: 'mock joke 3', category: 'mock category 3', time: '23:00:00'}, 
                {text: 'mock joke 4', category: 'mock category 4', time: '24:00:00'}
            ]
        }
        return (
            <>
                {mockSavedJokes.updated.map((jokeObj, index) => (
                    <Joke key={"Mock " + index} text={jokeObj.text} category={jokeObj.category} time={jokeObj.time}/>
                ))}

                {savedJokes.updated.map((jokeObj, index) => (
                    <Joke key={"Saved " + index} text={jokeObj.text} category={jokeObj.category} time={jokeObj.time}/>
                ))}
            </>
        )
    }

    const wrapper = mount(
        <AuthProvider>
            <TestComponent />
        </AuthProvider>
    )

    expect(wrapper.find('[data-test-id="joke"]').first().text()).toEqual('mock joke 3 ...')

    wrapper.find('[data-test-id="joke"]').first().simulate('click')

    expect(wrapper.find('[data-test-id="joke"]').first().text() === wrapper.find('[data-test-id="joke"]').last().text())

})

it('reorders saved Jokes by category ascending', () => {
    const TestJokeComponent = () => {
        const {savedJokes} = React.useContext(UserContext)

        const mockSavedJokes = {
            old: [
                {text: 'mock joke 1', category: 'mock category 1', time: '21:00:00'}, 
                {text: 'mock joke 2', category: 'mock category 2', time: '22:00:00'}
            ], 
            updated: [
                {text: 'mock joke 3', category: 'mock category 3', time: '23:00:00'}, 
                {text: 'mock joke 4', category: 'mock category 4', time: '24:00:00'},
                {text: 'mock joke 5', category: 'mock category 5', time: '01:00:00'},
                {text: 'mock joke 6', category: 'mock category 6', time: '02:00:00'},

            ]
        }

        return (
            <>
                {mockSavedJokes.updated.map((jokeObj, index) => (
                    <Joke key={"Mock " + index} text={jokeObj.text} category={jokeObj.category} time={jokeObj.time}/>
                ))}
            </>
        )
    }

    const wrapper = mount(
        <AuthProvider>
            <TestJokeComponent/>
            <Jokes/>
        </AuthProvider>
    )

    expect(wrapper.find('[data-test-id="joke"]').first().text()).toEqual('mock joke 3 ...')

    wrapper.find('[data-test-id="joke"]').first().simulate('click')
    wrapper.find('[data-test-id="joke"]').last().simulate('click')
    wrapper.find('[data-test-id="joke"]').at(1).simulate('click')
    wrapper.find('[data-test-id="joke"]').at(2).simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')

    wrapper.find('[data-test-id="orderCatAsc"]').simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')

})

it('reorders saved Jokes by category descending', () => {
    const TestJokeComponent = () => {
        const {savedJokes} = React.useContext(UserContext)

        const mockSavedJokes = {
            old: [
                {text: 'mock joke 1', category: 'mock category 1', time: '21:00:00'}, 
                {text: 'mock joke 2', category: 'mock category 2', time: '22:00:00'}
            ], 
            updated: [
                {text: 'mock joke 3', category: 'mock category 3', time: '23:00:00'}, 
                {text: 'mock joke 4', category: 'mock category 4', time: '24:00:00'},
                {text: 'mock joke 5', category: 'mock category 5', time: '01:00:00'},
                {text: 'mock joke 6', category: 'mock category 6', time: '02:00:00'},

            ]
        }

        return (
            <>
                {mockSavedJokes.updated.map((jokeObj, index) => (
                    <Joke key={"Mock " + index} text={jokeObj.text} category={jokeObj.category} time={jokeObj.time}/>
                ))}
            </>
        )
    }

    const wrapper = mount(
        <AuthProvider>
            <TestJokeComponent/>
            <Jokes/>
        </AuthProvider>
    )

    expect(wrapper.find('[data-test-id="joke"]').first().text()).toEqual('mock joke 3 ...')

    wrapper.find('[data-test-id="joke"]').first().simulate('click')
    wrapper.find('[data-test-id="joke"]').last().simulate('click')
    wrapper.find('[data-test-id="joke"]').at(1).simulate('click')
    wrapper.find('[data-test-id="joke"]').at(2).simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')

    wrapper.find('[data-test-id="orderCatDesc"]').simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')

})

it('reorders saved Jokes by date ascending', () => {
    const TestJokeComponent = () => {
        const {savedJokes} = React.useContext(UserContext)

        const mockSavedJokes = {
            old: [
                {text: 'mock joke 1', category: 'mock category 1', time: '21:00:00'}, 
                {text: 'mock joke 2', category: 'mock category 2', time: '22:00:00'}
            ], 
            updated: [
                {text: 'mock joke 3', category: 'mock category 3', time: '23:00:00'}, 
                {text: 'mock joke 4', category: 'mock category 4', time: '24:00:00'},
                {text: 'mock joke 5', category: 'mock category 5', time: '01:00:00'},
                {text: 'mock joke 6', category: 'mock category 6', time: '02:00:00'},

            ]
        }

        return (
            <>
                {mockSavedJokes.updated.map((jokeObj, index) => (
                    <Joke key={"Mock " + index} text={jokeObj.text} category={jokeObj.category} time={jokeObj.time}/>
                ))}
            </>
        )
    }

    const wrapper = mount(
        <AuthProvider>
            <TestJokeComponent/>
            <Jokes/>
        </AuthProvider>
    )

    expect(wrapper.find('[data-test-id="joke"]').first().text()).toEqual('mock joke 3 ...')

    wrapper.find('[data-test-id="joke"]').first().simulate('click')
    wrapper.find('[data-test-id="joke"]').last().simulate('click')
    wrapper.find('[data-test-id="joke"]').at(1).simulate('click')
    wrapper.find('[data-test-id="joke"]').at(2).simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')

    wrapper.find('[data-test-id="orderDateAsc"]').simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')

})

it('reorders saved Jokes by date ascending', () => {
    const TestJokeComponent = () => {
        const {savedJokes} = React.useContext(UserContext)

        const mockSavedJokes = {
            old: [
                {text: 'mock joke 1', category: 'mock category 1', time: '21:00:00'}, 
                {text: 'mock joke 2', category: 'mock category 2', time: '22:00:00'}
            ], 
            updated: [
                {text: 'mock joke 3', category: 'mock category 3', time: '23:00:00'}, 
                {text: 'mock joke 4', category: 'mock category 4', time: '24:00:00'},
                {text: 'mock joke 5', category: 'mock category 5', time: '01:00:00'},
                {text: 'mock joke 6', category: 'mock category 6', time: '02:00:00'},

            ]
        }

        return (
            <>
                {mockSavedJokes.updated.map((jokeObj, index) => (
                    <Joke key={"Mock " + index} text={jokeObj.text} category={jokeObj.category} time={jokeObj.time}/>
                ))}
            </>
        )
    }

    const wrapper = mount(
        <AuthProvider>
            <TestJokeComponent/>
            <Jokes/>
        </AuthProvider>
    )

    expect(wrapper.find('[data-test-id="joke"]').first().text()).toEqual('mock joke 3 ...')

    wrapper.find('[data-test-id="joke"]').first().simulate('click')
    wrapper.find('[data-test-id="joke"]').last().simulate('click')
    wrapper.find('[data-test-id="joke"]').at(1).simulate('click')
    wrapper.find('[data-test-id="joke"]').at(2).simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')

    wrapper.find('[data-test-id="orderDateDesc"]').simulate('click')

    expect(wrapper.find('[data-test-id="joke-history-item"]').at(1).text()).toEqual(' Joke: \"mock joke 3\"   Category:\"mock category 3\"   Time:\"23:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(0).text()).toEqual(' Joke: \"mock joke 4\"   Category:\"mock category 4\"   Time:\"24:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(3).text()).toEqual(' Joke: \"mock joke 5\"   Category:\"mock category 5\"   Time:\"01:00:00\" ')
    expect(wrapper.find('[data-test-id="joke-history-item"]').at(2).text()).toEqual(' Joke: \"mock joke 6\"   Category:\"mock category 6\"   Time:\"02:00:00\" ')

})