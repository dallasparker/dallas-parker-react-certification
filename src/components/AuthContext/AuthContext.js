import React from 'react';

import { UserContext, sort } from '../../Helper/common'

export const AuthProvider = ({ children }) => {
  // #I've chosen to use a combined effort of functional component state management
  // and Context, to manage global data such as login status and joke history.

  // #Here, you can see useState hook in use
    const [loginStatus, setLoginStatus] = React.useState(false)
    const [savedJokes, setSavedJokes] = React.useState({old: [], updated: []})

  // #Here, we have a couple of arrow functions being stored in the Login and Logout const variables
    const login = () => {
        setLoginStatus(true)
    }

    const logout = () => {
        setLoginStatus(false)
    }

    const updateSavedJokes = (newJokeObj) => {
  // #Here, we have the filter array function in use
        const duplicateCheck = savedJokes.updated.filter( savedJoke => ( savedJoke.text === newJokeObj.text))
        if(duplicateCheck.length === 0){
          let oldJokeArray = savedJokes.updated
          oldJokeArray.push(newJokeObj)
          setSavedJokes({old: savedJokes, updated: oldJokeArray})
        }
    }

    const orderSavedJokes = (sortOrder) => {
        if (sortOrder === 'catAsc') {
            const newJokesArray = sort('category', 'ascending', savedJokes)
            setSavedJokes({old: savedJokes, updated: newJokesArray})
          } else if (sortOrder === 'catDesc') {
            const newJokesArray = sort('category', 'descending', savedJokes)
            setSavedJokes({old: savedJokes, updated: newJokesArray})
          } else if (sortOrder === 'dateAsc') {
            const newJokesArray = sort('date', 'ascending', savedJokes)
            setSavedJokes({old: savedJokes, updated: newJokesArray})
          } else if (sortOrder === 'dateDesc') {
            const newJokesArray = sort('date', 'descending', savedJokes)
            setSavedJokes({old: savedJokes, updated: newJokesArray})
          }
    }

    return (
        <UserContext.Provider value={{ 
            loginStatus, 
            login, 
            logout, 
            savedJokes, 
            updateSavedJokes, 
            orderSavedJokes,
        }}>
            {children}
        </UserContext.Provider>
    )
}

export default AuthProvider;

