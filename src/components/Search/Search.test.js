// *** Note for Testing ***
// *** I have created two easy CLI tools to use in the package.json ***
// *** One is the "npm run coverage" command, which runs a testing report with 'watch' disabled ***
// *** The other is "npm run slim-coverage" which runs a testing report excluding the index.js and serviceWorker.js files ***

import React from 'react';
import { mount } from 'enzyme';
import Search from './Search'

it('renders the submit button', () => {
    const wrapper = mount(
        <Search />
    )

    expect(wrapper.find('[data-test-id="submitSearch"]').text()).toEqual('Search')
})

it('renders the reset button', () => {
    const wrapper = mount(
        <Search />
    )

    expect(wrapper.find('[data-test-id="submitReset"]').text()).toEqual('Reset')
})

it('changes the input value onChange', () => {
    const wrapper = mount(
        <Search />
    )

    wrapper.find('[data-test-id="searchInput"]').simulate('change', {
        target: {
            name: 'search',
            value: 'test search input',
          },
    })

    expect(wrapper.find('[data-test-id="searchInput"]').prop('value')).toEqual("test search input")
})