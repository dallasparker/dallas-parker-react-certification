import React from 'react'
import { getJokesBySearchTerm, getDateTime } from '../../Helper/common'
import Joke from '../Jokes/Joke';

function init() {
    return {searchTerm: '', loading: false};
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'setSearchTerm':
            return {searchTerm: action.payload, loading: state.loading}
        case 'toggleLoading':
            return {searchTerm: state.searchTerm, loading: !state.loading}
        case 'resetJokes':
            return init(action.payload);
        default:
        throw new Error();
    }
}

export default function Search() {
    let initial = {searchTerm: '', loading: false}
    // #Here, I utilize both useReducer and useState hooks
    const [state, dispatch] = React.useReducer(reducer, initial, init)
    const [jokesArray, setJokes] = React.useState({jokes: []})

    const getJokes = async(searchTerm) => {
        dispatch({type: 'toggleLoading'})
        const jokes = await getJokesBySearchTerm(searchTerm)
        setJokes({ jokes: jokes })
        dispatch({type: 'toggleLoading'})
    }

    const handleReset = () => {
        dispatch({type: 'resetJokes'})
        setJokes({ jokes: [] })
    }

    return (
        <>
            <div>
                <input
                    data-test-id="searchInput"
                    name='search'
                    type='text'
                    value={state.searchTerm}
                    placeholder='Enter your search term here...'
                    onChange={(event) => { dispatch({type: 'setSearchTerm', payload: event.target.value}) } } 
                />
                <button data-test-id="submitSearch" onClick={() => getJokes(state.searchTerm)}>Search</button>
                <button data-test-id="submitReset" onClick={handleReset}>Reset</button>
            </div>
            
            {state.loading === true ? <div>Searching for Jokes...</div> : null}
            
            {jokesArray.jokes.map( (joke, index) => {
                return <Joke key={index} text={joke.value} category='search' time={getDateTime()}/>
            })}
        </>
    )
}
