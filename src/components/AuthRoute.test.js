import React from 'react';
import App from '../App'
import AuthRoute from './AuthRoute'
import TestComponent from './Jokes/Jokes'
import { shallow, mount } from 'enzyme'

it('redirects the user to login upon lack of credential', () => {
    const wrapper = mount(
        <App>
            <AuthRoute>
                <TestComponent/>
            </AuthRoute>
        </App>
    )

    expect(wrapper.find("[data-test-id='togglePasswordShow']").text()).toEqual(' Show Password ')
})