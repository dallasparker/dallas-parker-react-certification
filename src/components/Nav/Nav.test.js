// *** Note for Testing ***
// *** I have created two easy CLI tools to use in the package.json ***
// *** One is the "npm run coverage" command, which runs a testing report with 'watch' disabled ***
// *** The other is "npm run slim-coverage" which runs a testing report excluding the index.js and serviceWorker.js files ***

import React from 'react'
import renderer from 'react-test-renderer';
import { BrowserRouter as Router,} from "react-router-dom"

import Nav from './Nav'

it('renders correctly', () => {
    const tree = renderer.create(<Router><Nav/></Router>).toJSON();
    expect(tree).toMatchSnapshot();
  });