import React, {Component} from 'react';
import { Link } from "react-router-dom"
import './Nav.css'

export default class Nav extends Component {
    render() {
        return (
            <nav>
                <ul>
                    <li>
                        <Link data-testid="home-link" to="/">Home</Link>
                    </li>
                    <li>
                        <Link data-testid="category-link" to="/categories">Categories</Link>
                    </li>
                    <li>
                        <Link data-testid="search-link" to="/search">Search</Link>
                    </li>
                    <li>
                        <Link data-testid="jokes-link" to="/jokes">Jokes</Link>
                    </li>
                </ul>
            </nav>
        )
    }
}
