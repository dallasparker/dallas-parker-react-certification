import React from 'react'
import { UserContext } from '../../Helper/common'

// #This component is an example of a functional component
export default function Jokes() {
    // #Here, you can see useContext in use
    const context = React.useContext(UserContext)

    return (
        <>
            <h1>Jokes I've Viewed</h1>
            <button data-test-id='orderCatAsc' onClick={() => context.orderSavedJokes('catAsc')}>Sort By Category: Asc</button>
            <button data-test-id='orderCatDesc' onClick={() => context.orderSavedJokes('catDesc')}>Sort By Category: Dsc</button>
            <button data-test-id='orderDateAsc' onClick={() => context.orderSavedJokes('dateAsc')}>Sort By Time: Asc</button>
            <button data-test-id='orderDateDesc' onClick={() => context.orderSavedJokes('dateDesc')}>Sort By Time: Dsc</button>

            {context.savedJokes.updated.map((jokeObj, index) => {
                return <div key={index} data-test-id="joke-history-item"> {`Joke: "${jokeObj.text}"   Category:"${jokeObj.category}"   Time:"${jokeObj.time}"`} </div>
            })}
        </>
           
    )
}
