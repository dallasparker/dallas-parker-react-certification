// *** Note for Testing ***
// *** I have created two easy CLI tools to use in the package.json ***
// *** One is the "npm run coverage" command, which runs a testing report with 'watch' disabled ***
// *** The other is "npm run slim-coverage" which runs a testing report excluding the index.js and serviceWorker.js files ***

import React from "react";
import { shallow } from 'enzyme';
import Joke from './Joke'

it('renders a truncated joke when props are present', () => {
    const JokeComponent = shallow(<Joke text='test joke that is long and needs to be truncated for brevity' category='test category' time='04:36:00' />);
    expect(JokeComponent.find('div[data-test-id="joke"]').text()).toEqual('test joke that is long and needs to be truncated f ...')
})