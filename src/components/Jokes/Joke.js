import React from 'react'
import { UserContext } from '../../Helper/common'

// #This component is an example of a functional component
// #Props are passed in here
export default function Joke( text, category, time ) {
    // #Here, I again utilize useContext hook to consume context provided in AuthContext
    const context = React.useContext(UserContext)
    // #Here, an example of functional component state declaration
    const [truncated, setTruncated] = React.useState(true)

    const handleClick = () => {
        // #This is a good example of a closure
        // #Here we have access to "text", "setTruncated()", "truncated", etc
        // #Because closures extend scope to the parent level (in this case, one level above handleClick())
        setTruncated(!truncated)
        context.updateSavedJokes(text, category, time)
    }

    

    return (
            <div data-test-id={"joke"} onClick={() => handleClick()}>
                {
                    truncated === true ? `${text.text.slice(0, 50)} ...` 
                    :
                    text.text
                }
            </div>
    )
}
