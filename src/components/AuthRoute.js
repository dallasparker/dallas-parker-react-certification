import React from 'react';
import { Route } from "react-router-dom"
import { UserContext } from '../Helper/common'
import Login from './Login/Login'

export default function AuthRoute({component: Component, ...rest}) {
    return (
        <UserContext.Consumer>
            {value => {
            return <Route
                // #Here, we use the spread operator to expand and pass along the remaining props of the component
                {...rest}
                render={(props) => {
                    if (value.loginStatus === true) {
                        return <Component {...props} />
                    }
                    return <Login />
                }}
            />
            }}
            
        </UserContext.Consumer>
    )
}
