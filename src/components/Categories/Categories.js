import React, { Component } from 'react'
import styled from 'styled-components';
import { getJokeCategories, getJokeByCategory, getDateTime } from '../../Helper/common'
import Joke from '../Jokes/Joke';

// #This component is an example of a class component
export default class Categories extends Component {
    constructor(props) {
        super(props);
// #Here is an example of "this" keyword scoping being used
// #Also, class component state being declared in the constructor
        this.state = { categories: [], selectedJoke: '', selectedCategory: '', hoverLink: false};
        this.handleCategoryClick = this.handleCategoryClick.bind(this);
    }

// #Here is an example of using the "async await" pattern for promise resolution
// #Also, this is a good example of the componentDidMount lifecycle method
    async componentDidMount() {
        const categories = await getJokeCategories()
        this.setState({ 
            categories: categories, 
            selectedJoke: this.state.selectedJoke,
            selectedCategory: this.state.selectedCategory,
        })
    }

    // #componentDidUpdate() {
    //     ... some code to occur immediately after updating
    // }

    // #componentDidCatch() {
    //     ... some code to handle caught exceptions thrown by descendents
    // }

    async handleCategoryClick(categoryName) {
        const randomJoke = await getJokeByCategory(categoryName)
// #Again, here is an example of "this" keyword scoping being used in another function
        this.setState( { categories: this.state.categories, selectedJoke: randomJoke.data.value, selectedCategory: categoryName})
    }

    render() {
        // To switch things up, I decided to import a little third party package to add hover to the category names.
        // Emotion.js works similarly as a package alternative
        const HoverText = styled.p`
            color: #000;
            :hover {
                color: #ed1212;
                cursor: pointer;
            }
        `
        return (
            this.state.categories.length > 0 
            ? 
                <>
                    <h1 className="categoryHeader">
                        Random Joke By Category:
                    </h1>
                    <ul>
                        {/* #Here, we have the map array function in use */}
                        {this.state.categories.map( (cat, index) => {
                            return <HoverText><li key={index} onClick={() => this.handleCategoryClick(cat)}>{cat}</li></HoverText>
                        })}
                    </ul>

                    {this.state.selectedJoke !== '' && this.state.selectedCategory !== '' ? 
                    <Joke 
                        text={this.state.selectedJoke}
                        category={this.state.selectedCategory} 
                        time={getDateTime()}
                    /> : null}
                </>
            :
                <div className="loader">Loading Categories...</div>           
        )
    }
}
