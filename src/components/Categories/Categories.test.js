// *** Note for Testing ***
// *** I have created two easy CLI tools to use in the package.json ***
// *** One is the "npm run coverage" command, which runs a testing report with 'watch' disabled ***
// *** The other is "npm run slim-coverage" which runs a testing report excluding the index.js and serviceWorker.js files ***

import React from "react";
import Enzyme from "enzyme";
import Categories from "./Categories";
import Adapter from 'enzyme-adapter-react-16';

beforeAll(() => {
    Enzyme.configure({ adapter: new Adapter() })
    global.fetch = jest.fn();
});

let container;

beforeEach(() => {
    container = Enzyme.shallow(<Categories />, { disableLifecycleMethods: true });
});

afterEach(() => {
    container.unmount();
});

it("must render a loading span before api call success", () => {
    expect(container.find("div.loader").exists()).toBeTruthy();
});

it("must show the categories and hide the loading span after api call success", (done) => {
    const spyDidMount = jest.spyOn(Categories.prototype,"componentDidMount");

    fetch.mockImplementation(() => {
        return Promise.resolve({
            status: 200,
            json: () => {
            return Promise.resolve(["animal", "career", "celebrity", "dev"]);
            }
        });
    });
    const didMount = container.instance().componentDidMount();
    expect(spyDidMount).toHaveBeenCalled();

    didMount.then(() => {
        container.update();
        expect(container.find("h1.categoryHeader").text()).toContain("Random Joke By Category:");
        expect(container.find("div.loader").length).toBe(0);
        spyDidMount.mockRestore();
        fetch.mockClear();
        done();
    })
})