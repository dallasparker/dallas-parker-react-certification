Notes

# Helper > common.js >

## fakeAuthenticationRequest

`The login button should be disabled until a username and password have been entered`
[] email validation is not working as expected (a single letter passes the validation if password validates)

x the match results in true if either email or password is correct; for authentication both need to be correct
x [not required] display an indicator that the process is in action and then terminate when action resolves
*** Note: In the Login component, I display `<div data-test-id="loadingIndicator">Loading...</div>` ***
*** Is this what is expected? ***

## getJokeCategories/getJokeByCategory/getJokesBySearchTerm

x great use of async/await
x nice use of arrow functions
- [not required] would like to see a .catch()
- [not required] would like to see use of .finally()
- when fetching data in an async function it's a better user experience to start and end a loading indicator so the user knows the app is working on their action/request
*** because these functions cannot be tested directly, adding a catch / finally block would artificially ***
*** decrease the test coverage percentage (despite the fact that we do indeed test mocked versions ***
*** of these functions) I've added a catch block to demonstrate how I would catch and throw error data ***
*** in a real environment ***
*** Note: In the Search component, I display `<div>Searching for Jokes...</div>` ***
*** Is this what is expected? ***

# Home

## Navigation

x [not required] would have been nice to click on the headings on the home page to go to each page in addition to the navigation menu

# Categories

## map

- [50] when using map you should allow use React `keys`
*** The categories component already seemed to have keys with each map iteration, ***
*** however, I did notice the Joke component missing keys with it's map function, so I added them ***

## styles

x there was no styling used in this component
x there should be some visual que that the category names are links to be clicked or interacted with (often the use of a pointer is used)
x in the absence of visual ques there should be some text to tell the user what is expected or how to interact with the page

# Login

## Validations

x email/password validations are not working as expected
x error counter only shows number, doesn't say what the number is for (i.e. `failed attempts: ${count}`)
x error counter doesn't seem to increment
x should have some styling for error validation messages (typically done in red)

## handleCredentialChange, handleRadioButtonChange

x if using if/else if/else statement you should have the final else case; if not consider using a ternary statement
  - `event.target.name === 'username' ? setNameState('username') : setNameState('password')`
  - `setSelectedRadioButton(event.target.value === 'login' ? 'login' : 'signUp')`
x if not using the final else case there is a chance that a use case is getting missed, and edge case to be considered

## handleSubmit

- this method is getting large and nested
- because of the method structure it is beginning to get difficult to follow
- there are several lines of code that are repeated between the two if/else if statements (`setErrorState()`, `setLoading()`)
- [best practice] refactor into a function that accepts the differences as parameters (i.e. failed message, count); if not a single function consider using 2 functions so that actions are clear and reduce the nesting
*** Good point. I started to notice it was a little overly nested during the testing phase. ***
*** I've actually set a goal to be better at testing components as I go, rather than at the end, or all at once. ***
*** I expect that to help me catch these kinds of things early and often, in the future. ***

# Search

## Reducer case names

x `setLoading` is not as clear as it could be; in this case `toggleLoading` would probably be a better name for clarity