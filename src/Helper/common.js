import React from 'react';
import Axios from 'axios'
import json from '../Helper/userJson.json'

export const UserContext = React.createContext()

export const getDateTime = () => {
    const today = new Date()
    const date = `${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()}`
    const time = `${today.getHours()}: ${today.getMinutes()}: ${today.getSeconds()}`
    return `${date} ${time}`;
}

export const sort = (sortBy, direction, savedJokes) => {
    if (sortBy === 'category') {
        if (direction === 'ascending') {
            return savedJokes.updated.sort((item, nextItem) => (item.category > nextItem.category ? 1 : -1))
        } else if (direction === 'descending') {
            return savedJokes.updated.sort((item, nextItem) => (item.category < nextItem.category ? 1 : -1))
        }
    } else if (sortBy === 'date') {
        if (direction === 'ascending') {
            return savedJokes.updated.sort((item, nextItem) => (item.time > nextItem.time ? 1 : -1))
        } else if (direction === 'descending') {
            return savedJokes.updated.sort((item, nextItem) => (item.time < nextItem.time ? 1 : -1))
        }
    }
}
export const containsNoEmailSymbol = (string) => {
    return (!/([@])/.test(string))
}

export const containsNoUpperCase = (string) => {
    const test = string.toLowerCase() === string
    return test
}

export const containsNoSpecialCharacter = (string) => {
    const test = (!/([!@#$%^&*])/.test(string))
    return test
}

export const containsNoNumbers = (string) => {
    const test = (!/([1-9])/.test(string));
    return test
}

export const lengthIsNotValid = (string) => {
    if ( string.length < 6 ) {
        return true
    } else if (string.length > 11) {
        return true
    } else {
        return false
    }
}

export const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

export const fakeAuthenticationRequest = async (email, password) => {
    await sleep(2000);
    const matchEmail = json.users.some(e => e.email === email)
    const matchPassword = json.users.some(e => e.password === password)
    if ( matchEmail && matchPassword === true ) {
        return true
    } else {
        return false
    }
}

export const fakeSignUpRequest = async (email, password) => {
    let oldJson = {
        users: []
    }
    await sleep(2000)

    oldJson.users.push({email: email, password: password})
    const newJson = JSON.stringify(oldJson)
    
    /* #If we were using some backend, such as node, we might utilize a tool like File-System to read our old json object *
     * and write our new email and password to said object. To keep the app simple, I will simply include a code example,*
     * and write our new sign up credentials in a JSON object to be passed back and simulate a "successful request"      */

    //     fs.writeFile(json, newJson, (err) => {
    //         if(err) {
    //             alert("Uh oh, there was a problem! See your console for error info")
    //         } else { 
    //             return true 
    //         }
    //     })

    return newJson
}

export const getJokeCategories = async () => {
    return Axios.get('https://api.chucknorris.io/jokes/categories')
        .then(response => {return response.data})
        // .catch(throw new Error here, to be caught above...)
        // .finally(code to be executed after all else, in every case)
        // because these functions cannot be tested directly, adding a catch / finally block would artificially 
        // decrease the test coverage percentage (despite the fact that we do indeed test mocked versions
        // of these functions) I've added a catch block to demonstrate how I would catch and throw error data
        // in a real environment
}

export const getJokeByCategory = async (categoryName) => {
    return Axios.get(`https://api.chucknorris.io/jokes/random?category=${categoryName}`)
        .then(response => {return response})
        // .catch(throw new Error here, to be caught above...)
        // .finally(code to be executed after all else, in every case)
        // because these functions cannot be tested directly, adding a catch / finally block would artificially 
        // decrease the test coverage percentage (despite the fact that we do indeed test mocked versions
        // of these functions) I've added a catch block to demonstrate how I would catch and throw error data
        // in a real environment
}

export const getJokesBySearchTerm = async (string) => {
    return Axios.get(`https://api.chucknorris.io/jokes/search?query=${string}`)
        .then(response => {return response.data.result})
        // .catch(throw new Error here, to be caught above...)
        // .finally(code to be executed after all else, in every case)
        // because these functions cannot be tested directly, adding a catch / finally block would artificially 
        // decrease the test coverage percentage (despite the fact that we do indeed test mocked versions
        // of these functions) I've added a catch block to demonstrate how I would catch and throw error data
        // in a real environment
}